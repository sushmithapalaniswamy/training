
package com.kpr.training.education.people;

import com.kpr.training.education.college.College;


import java.util.Scanner;

public class People {
	
	public int id ;
	protected String name ;
	private void printDetails(String name , int id) {
		System.out.println("protected");
		System.out.println("name:" + name);
		System.out.println("id:" +id);
	}
	
    void defaultMethod(String name , int id) {
        System.out.println("Default");
	    System.out.println("name:" + name);
	    System.out.println("id:" +id);	
	}
	
    public static void main(String[] args) {
		
		People people = new People();
		College college = new College();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the name:");
		String name = scanner.nextLine();
		System.out.println("Enter the id:");
		int id = scanner.nextInt();
		people.printDetails(name, id);
		people.defaultMethod(name , id);
		scanner.close();
		System.out.println("college name:" + college.collegeName);
		
	}
	
	
	

}
