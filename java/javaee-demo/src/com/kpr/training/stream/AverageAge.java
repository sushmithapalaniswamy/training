/*
 * Requirement:
 *      Write a program to find the average age of all the Person in the person List
 * Entity:
 *      AverageAge
 *      Person
 * Function Declaration:
 *      public static void main(String[] args)
 * Jobs to be done:
 *     1)Invoke the method createRoater from Person and store it in list.
 *     2)Declare averageAge variable as double.
 *       2.1)For each person
 *           2.1.1)Invoke the method getAge from person.
 *           2.1.2)Find the average of the  age.
 *           2.1.3)Store it as double.
 *     3)print the averageAge
 *
 *pseudo code:
 		public class AverageAge {
	
			public static void main(String[] args) {
	
				List<Person> list = Person.createRoster();

					double averageAge = (find the average of the age)

				System.out.println("Average age : " + averageAge);
	}
}
 */
package com.kpr.training.stream;

import java.util.List;

public class AverageAge {
	
	public static void main(String[] args) {
	
		List<Person> list = Person.createRoster();

		double averageAge = list
				.stream()
				.mapToInt(Person::getAge)
				.average()
				.getAsDouble();

		System.out.println("Average age : " + averageAge);
	}
}