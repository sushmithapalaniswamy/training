select student.id
      ,student.name
      ,student.class
      ,mark.social
      ,mark.science
      ,mark.maths
	  ,mark.tamil
      ,mark.english
      ,mark.total
      ,teacher.name
  from student  
  inner join mark on student.id = mark.std_id 
  inner join teacher on student.teacher_id=teacher.id;