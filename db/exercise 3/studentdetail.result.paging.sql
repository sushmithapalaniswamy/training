SELECT student.roll_number
      ,student.name
      ,student.dob
      ,student.gender
      ,student.email
      ,student.phone
      ,student.address
      ,semester_result.grade
      ,semester_result.credits
      ,semester_result.gpa
      ,semester_Result.semester
      ,college.name
      ,department.dept_name
  FROM student
       INNER JOIN semester_result 
       ON student.id = semester_result.stud_id
       INNER JOIN college_department
       ON student.cdept_id = college_department.cdept_id
       INNER JOIN college
       ON student.college_id = college.id
       INNER JOIN  department
       ON department.dept_code = college_department.udept_code
 ORDER BY college.name,semester_result.semester
 LIMIT 0,20;
  
      
    
    
