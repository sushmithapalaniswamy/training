package com.kpr.training.reflections;

/*
 Requirement:Explain concept of loading and reloading.
 
 Answer:
 Loading:All classes in a Java application are loaded using some subclass of java.lang.ClassLoader.
 	     Loading classes dynamically must therefore also be done using a java.lang.ClassLoader subclass.
         When class is loaded, all classes it references are loaded too. This class loading 
         pattern happens recursively, until all classes needed are loaded.
Reloading:Java's builtin Class loaders always checks if a class is already loaded before loading 
		  it. Reloading the class is therefore not possible using Java's builtin class loaders. 
		  To reload a class you will have to implement your own ClassLoader subclass .Everytime
		   you want to reload a class you must use a new instance of your ClassLoader subclass


*/