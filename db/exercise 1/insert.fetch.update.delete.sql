CREATE TABLE `db`.`student` (
             `id` INT NOT NULL
			,`name` VARCHAR(45) NOT NULL
			,`class` INT NULL DEFAULT 10
			,`age` INT NULL 
			,`teacher_id` INT NULL,
            PRIMARY KEY (`id`),
            INDEX `teacher_id_idx` (`teacher_id` ASC) VISIBLE,
            CONSTRAINT `f_id`
               FOREIGN KEY (`teacher_id`)
            REFERENCES `db`.`teacher` (`id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION);
INSERT INTO `db`.`student` (`id`,`name`,`teacher_id`,`age`)
	 VALUES  ('1','shibi','1','16');
INSERT INTO `db`.`student` (`id`,`name`,`teacher_id`,`age`) 
	 VALUES ('2','kavi','2','15');
INSERT INTO `db`.`student` (`id`, `name`, `teacher_id`,`age`) 
     VALUES ('3', 'paramesh', '3','15');
INSERT INTO `db`.`student` (`id`, `name`, `teacher_id`,`age`) 
     VALUES ('4', 'rithick', '4','16');
INSERT INTO `db`.`student` (`id`, `name`, `teacher_id`,`age`) 
     VALUES ('5', 'nivetha', '1','15');
INSERT INTO `db`.`student` (`id`, `name`, `teacher_id`,`age`) 
     VALUES ('6', 'swetha', '2','17');
INSERT INTO `db`.`student` (`id`, `name`, `teacher_id`,`age`) 
     VALUES ('7', 'sushmitha', '3','15');
INSERT INTO `db`.`student` (`id`, `name`, `teacher_id`,`age`) 
     VALUES ('8', 'teja', '4','15');
INSERT INTO `db`.`student` (`id`, `name`, `teacher_id`,`age`) 
     VALUES ('9', 'mithesh', '1','16');
INSERT INTO `db`.`student` (`id`, `name`, `teacher_id`,`age`) 
     VALUES ('10', 'ria', '4','15');
INSERT INTO `db`.`student` (`id`,`name`, `teacher_id`,`age`) 
     VALUES ('11','suriya', '3','15');
       DESC  db.student;
     SELECT student.id 
	       ,student.name 
		   ,student.class 
		   ,student.teacher_id 
       FROM db.student ;
     SELECT name 
       FROM db.student;
     UPDATE student
        SET age = 16 
      WHERE id = 11 ;
     SELECT id 
	       ,name 
		   ,teacher_id 
		   ,age
      FROM db.student ;
    DELETE FROM student
     WHERE id = 11 ;
    SELECT id 
	     ,name 
		 ,teacher_id 
		 ,age
      FROM db.student ;


