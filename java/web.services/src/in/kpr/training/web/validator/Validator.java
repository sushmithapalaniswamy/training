package in.kpr.training.web.validator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.kpr.training.web.exception.AppException;
import in.kpr.training.web.exception.ErrorCode;


/**
 * Problem Statement
 * 1.Create a validator Class to validate the person and date
 * 
 * Entity
 * 1.Validator
 * 2.AppException
 * 3.ErrorCode
 * 
 * Method Signature
 * 1.public static Date dateChanger(String date);
 * 
 * Jobs to be Done
 * 1.Create a method called dateChanger with string paramter
 * 2.Create a SimpleDateFormat object called dateFormat with "dd-MM-yyyy" pattern
 * 3.Parse the LocalDate with "dd-MM-yyyy"
 * 4.return the parsed date with dateFormat
 * 5.Catch the Exception
 * 
 * Pseudo Code
 * public class Validator {
 *  
 *  public static Date dateChanger(String date) {
 *      
 *      try {
 *          SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
 *          
 *          LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy")
 *                  .withResolverStyle(ResolverStyle.STRICT));
 *          
 *          return (Date) dateFormat.parse(date);
 *      } catch (Exception e ) {
 *          throw new AppException(ErrorCodes.E427, e);
 *      }
 *  }
 *}
 * 
 *
 */

public class Validator {
    
    /*public static Date dateChanger(String date) {
        
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            
            LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy")
                    .withResolverStyle(ResolverStyle.STRICT));
            
            return (Date) dateFormat.parse(date);
        } catch (Exception e ) {
            throw new AppException(ErrorCodes.E427, e);
        }
    }*/
    
    public static java.sql.Date dateChanger(String date) {
        try {
            java.util.Date dateFormat = new SimpleDateFormat("dd-MM-yyyy").parse(date);
            java.sql.Date sqlDate = new java.sql.Date(dateFormat.getTime());
            return sqlDate;
        } catch (Exception e ) {
            throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
        }
    }
    
    public static Date dateConverter(String date) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date newDate = null;
        dateFormat.setLenient(false);
        
        try {
            dateFormat.parse(date);
            
            newDate = dateFormat.parse(date);
        } catch (Exception e) {
            throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
        }
        return newDate;
    }
}
