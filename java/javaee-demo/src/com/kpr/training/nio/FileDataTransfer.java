/*
Requirement:
    To create two files named source and destination . Transfer the data from source file to 
    destination file using NIO file channel.
    
Entity:
    FileDataTransfer
    
Function Declaration:
     public static void main(String[] args) throws Exception{}
     
Jobs to be done:
    1)Specify the path of the file and give the access to the file in read write mode and store it in sourceFile.
    2)Create a channel for the sourceFile named sourceChannel.
    3)Specify the path of the file and give the access to the file in read write mode and store it in destinationFile.
    4)Create a channel for the destination file named destinationChannel .
    5)store the size of the file in count.
    6)Transfer the data from the source file to destination file.
    
    
Pseudo code:
class FileDataTransfer {
	
    public static void main(String[] args) throws Exception {
    	

        RandomAccessFile sourceFile = new RandomAccessFile("C:\\1dev\\training\\java\\javaee-demo\\src\\com\\kpr\\training\\nio\\source.txt","rw");
        FileChannel sourceChannel = sourceFile.getChannel();

        RandomAccessFile destinationFile = new RandomAccessFile("C:\\1dev\\training\\java\\javaee-demo\\src\\com\\kpr\\training\\nio\\destination.txt","rw");
        FileChannel destinationChannel = destinationFile.getChannel();
        
        long count = sourceChannel.size();

        destinationChannel.transferFrom(sourceChannel, 0, count);
        System.out.println("Data Transferred to Destination file");
        sourceFile.close();
        destinationFile.close();
    }
}

*/

package com.kpr.training.nio;

import java.nio.channels.FileChannel;
import java.io.RandomAccessFile;

public class FileDataTransfer {
	
    public static void main(String[] args) throws Exception {
    	

        RandomAccessFile sourceFile = new RandomAccessFile("C:\\1dev\\training\\java\\javaee-demo"
        		+ "\\src\\com\\kpr\\training\\nio\\source.txt","rw");
        FileChannel sourceChannel = sourceFile.getChannel();

        RandomAccessFile destinationFile = new RandomAccessFile("C:\\1dev\\training\\java\\javaee-demo"
        		+ "\\src\\com\\kpr\\training\\nio\\destination.txt","rw");
        FileChannel destinationChannel = destinationFile.getChannel();

        long count = sourceChannel.size();

        destinationChannel.transferFrom(sourceChannel, 0, count);
        System.out.println("Data Transferred to Destination file");
        sourceFile.close();
        destinationFile.close();
    }
}
