/*Requirement: To find the previous friday when given with a date.
 * 
 * Entity: PreviousFriday
 * 
 * Function Declaration: public static void main(String[] args);
 * 
 * Jobs to be done:
 * 		1. Assign the input date to date variable of type LocalDate.
 * 		2. Find the previousFriday from the given date and print the previousFriday.
 * 
 * PseudoCode:
 * 
public class PreviousFriday {
	
	public static void main(String[] args) {
		
		LocalDate date = 2020-10-02; 
		System.out.printf("The previous Thursday is: ", previous friday from date));
	}
}

 * */
package com.kpr.training.dateandtime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.*;

public class PreviousFriday {
	
	public static void main(String[] args) {
		
		LocalDate date = LocalDate.of(2020, 10, 1);
		System.out.printf("The previous Friday is %s%n", date.with(TemporalAdjusters
				          .previous(DayOfWeek.FRIDAY)));
	}
}