/*Requirement:
 *   Write a program to add 5 elements to a xml file and print the elements in the xml file using list. 
 *   Remove the 3rd element from the xml file and print the xml file.
 *   
 * Entities:
 *     XmlDemo
 * Function Signature:
 * 
 *    public static void main(String[] args)
 *    
 * Jobs to be done:
 *   1) Create a object for Property named property.
 *   2) Add elements to the property
 *   3) Create a object for FileOutputStream and assign a name for xml file.
 *   4) Store the property in FileOutputStream as xml file.
 *   5) Print the elements in xml file using list. 
 *   6) Remove the 3rd element from the xml file.
 *   7)Prin the elements in the xml file.
 *   
 *Pseudo code:
 *
 *class XmlProperties {
 *
      public static void main(String[] args) throws IOException {
        
        Properties property = new Properties();
        //Add the element to the property.
        
        OutputStream output = new FileOutputStream("Number.xml");
		property.storeToXML(output, null);
        
        property.list(System.out);

        property.remove("3rd element key");
        property.list(System.out);
        }
  }   
        
 */
package com.kpr.training.properties;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class XmlDemo {

	public static void main(String[] args) throws IOException {
		
		Properties property = new Properties();
		property.setProperty("1", "One");
		property.setProperty("2", "Two");
		property.setProperty("3", "Three");
		property.setProperty("4", "Four");


		OutputStream output = new FileOutputStream("Number.xml");
		property.storeToXML(output, null);
		System.out.println("Properties stored in xml file successfully");

		property.list(System.out);

		property.remove("3");
		System.out.println("After removing the element from the xml file ");
		property.list(System.out);

	}

}