/*
Requirement:
    In the following program, called ComputeResult, what is the value of result after each numbered line executes?
    public class ComputeResult {
        public static void main(String[] args) {
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');

    /1/     result.setCharAt(0, original.charAt(0));
    /2/     result.setCharAt(1, original.charAt(original.length()-1));
    /3/     result.insert(1, original.charAt(4));
    /4/     result.append(original.substring(1,4));
    /5/     result.insert(3, (original.substring(index, index+2) + " "));

            System.out.println(result);
        }
    }
Entity:
    ComputeResult

Function declaration:
    public static void main(String[] args)
*/
/*
Output:
1) si
2) se
3) swe 
4) sweoft
5) swear oft
*/

package com.kpr.training.java.string;

public class ComputeResult {
        
        public static void main(String[] args) {
            
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');
            result.setCharAt(0, original.charAt(0));//result:si
            result.setCharAt(1, original.charAt(original.length()-1));//result:se
            result.insert(1, original.charAt(4));//result:swe
            result.append(original.substring(1,4));//result:sweoft
            result.insert(3, (original.substring(index, index+2) + " "));//result:swear oft
            System.out.println(result);
        }
    }

