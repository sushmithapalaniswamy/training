/*
Requirements:
  - Private Constructor with main class. 
Entity:
  - Private Constructor
  - Test
Function Declaration:
  - public static void main(String[] args)
Jobs to be done:
    1)Invoke the method instanceMethod of class Test
       2)create a constructor 
          2.1)print as "private constructor".
Pseudo Code:
class Test { 
    private Test() {
        System.out.println("This is a private constructor.");
    }
    public static void instanceMethod() { 
        Test obj = new Test();
    }
}
class PrivateConstructor {
    
    public static void main(String[] args) {
    
       // call the instanceMethod()
       Test.instanceMethod();
    }   
}
*/
package com.kpr.training.reflections;

class Test {
 
    private Test() {
        System.out.println("private constructor.");
    }

    public static void instanceMethod() { 
        Test test = new Test();//constructor
    }

}

class PrivateConstructor {

    public static void main(String[] args) {
       
       // call the instanceMethod()
       Test.instanceMethod();
    }
}
