/*
 * Requirement:
 *      To print minimal person with name and email address from the Person class using java.util.Stream<T>#map API by referring Person.java
 * 
 * Entity:
 *      MinimalPersonMap
 *      Person
 *
 * Function Signature:
 *      public static <R, T> void main(String[] args)
 *
 * Jobs To Be Done:
 *      1)Invoke the method createRoater from Person and store it in list.
 *      2)Create a ArrayList named name of type String.
 *          2.1)For each person
 *             2.1.1)Invoke the method getName and  map the name .
 *             2.1.2)Collect the name as list and store it in name.
 *     2)Create a ArrayList named mailId of type String.
 *          2.1)For each person
 *             2.1.1)Invoke the method getEmailAddress and  map the mailid .
 *             2.1.2)Collect the mailid as list and store it in mailId.
 *      4)Declare a variable minimalname of type String and store the minimum name from the list name.
 *      4)Declare a variable  minimalId of type String and store the minimum mailid from the list mailId.
 *      6)Print the minimalname and minimalId.
 *      
 * Pseudo code:
 * public class MinimalPersonMap {

    public static void main(String[] args) {
        List<Person> list = Person.createRoster();
        ArrayList<String> name = (ArrayList<String>) list.stream()
        		.map(s -> s.getName())
                .collect(Collectors.toList());
        ArrayList<String> mailId = (ArrayList<String>) list.stream()
        		.map(s -> s.getEmailAddress())
                .collect(Collectors.toList());
        String minimalName = Collections.min(name);
        String minimalId = Collections.min(mailId);

        System.out.println("The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    }
}
*/
package com.kpr.training.stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class MinimalPersonMap {

    public static void main(String[] args) {
        
    	List<Person> list = Person.createRoster();
        ArrayList<String> name = (ArrayList<String>) list.stream()
        		.map(s -> s.getName())
                .collect(Collectors.toList());
        ArrayList<String> mailId = (ArrayList<String>) list.stream()
        		.map(s -> s.getEmailAddress())
                .collect(Collectors.toList());
        String minimalName = Collections.min(name);
        String minimalId = Collections.min(mailId);

        System.out.println("The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    }
}