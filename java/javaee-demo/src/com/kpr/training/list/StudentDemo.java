/*Requirement:
 * 		LIST CONTAINS 10 STUDENT NAMES  krishnan, abishek, arun,vignesh, 
 *      kiruthiga, murugan,adhithya,balaji,vicky, priya  and display 
 *      only names starting with 'A'.
 *      
 *Entity:
 *      StudentDemo
 *      
 *Function Decalration:
 *      public statuc void main(String[] args) { }
 *      
 *Jobs To Be Done:
 *		1)Create a arraylist.
 *      2)Add the given names  to the list.
 *      3)For each name in the list
 *        3.1)if the name starts with 'A' , print the name.
 *        3.2)otherwise move to next iteration.
 *        
 *Pseudo code:
 *public class StudentDemo {
		
		public static void main(String[] args) {
   
    		List<String> list = new ArrayList<>();
        
        	Add elements to the list.
        
        	for (String name : list) {
            	if (name.startsWith("A")) {
                	System.out.println(name);
            	}
        	}
    	}
  }
 */
package com.kpr.training.list;

import java.util.ArrayList;
import java.util.List;

public class StudentDemo {
	
	public static void main(String[] args) {
   
    	List<String> list = new ArrayList<>();
        list.add("Krishnan");
        list.add("Abishek");
        list.add("Arun");
        list.add("Vignesh");
        list.add("Kiruthiga");
        list.add("Murugan");
        list.add("Adhithya");
        list.add("Balaji");
        list.add("Vicky");
        list.add("Priya"); 
        
        System.out.println("Name starts with 'A'");
        for (String name : list) {
            
        	if (name.startsWith("A")) {
                System.out.println(name);
            }
        }
    }
}
