CREATE TABLE `db`.`student1` (
            ,`id` INT NOT NULL
            ,`name` VARCHAR(45) NOT NULL
            ,`age` INT NOT NULL 
            ,`teacher_id` INT NULL
            ,PRIMARY KEY (`id`));
 INSERT INTO `db`.`student1` (`id`,`name`,`teacher_id`,`age`)
 VALUES ('1','shibi','1','16');
 INSERT INTO `db`.`student1` (`id`,`name`,`teacher_id`,`age`) 
 VALUES ('2','kavi','2','15');
 INSERT INTO `db`.`student1` (`id`,`name`,`teacher_id`,`age`) 
 VALUES ('3','paramesh','3','15');
 INSERT INTO `db`.`student1` (`id`,`name`,`teacher_id`,`age`) 
 VALUES ('4','rithick','4','16');
CREATE TABLE `db`.`student2` (
            ,`id` INT NOT NULL
            ,`name` VARCHAR(45) NOT NULL
            ,`age` INT NOT NULL 
            ,`teacher_id` INT NULL
            , PRIMARY KEY (`id`));
 INSERT INTO `db`.`student2` (`id`,`name`,`teacher_id`,`age`) 
 VALUES ('4','rithick','4','16');
 INSERT INTO `db`.`student2` (`id`,`name`,`teacher_id`,`age`) 
 VALUES ('5','nivetha','1','15');
 INSERT INTO `db`.`student2` (`id`,`name`,`teacher_id``age`) 
 VALUES ('6','swetha' '2','17');
 INSERT INTO `db`.`student2` (`id`,`name`,`teacher_id`,`age`) 
 VALUES ('7','sushmitha','3','15');
 INSERT INTO `db`.`student2` (`id`, `name`, `teacher_id`,`age`)
 VALUES ('8','teja', \'4','15');
SELECT student1.id 
      ,student1.name 
	  ,student1.age 
	  ,student1.teacher_id 
  FROM db.student1
 UNION DISTINCT
SELECT student2.id 
      ,student2.name 
	  ,student2.age 
	  ,student2.teacher_id 
   FROM db.student2;
 SELECT student1.id 
       ,student1.name 
	   ,student1.age 
	   ,student1.teacher_id 
   FROM db.student1
  UNION ALL
 SELECT student2.id 
       ,student2.name 
	   ,student2.age 
	   ,student2.teacher_id 
   FROM db.student2;
   DROP TABLE db.student1;
   DROP TABLE db.student2;