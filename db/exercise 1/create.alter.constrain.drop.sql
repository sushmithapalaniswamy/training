CREATE TABLE `db`.`student` (
             `id` INT NOT NULL,
             `name` VARCHAR(45) NOT NULL,
             `class` INT NULL DEFAULT 10,
             `teacher_id` INT NULL,
              PRIMARY KEY (`id`),
              INDEX `teacher_id_idx` (`teacher_id` ASC) VISIBLE,
              CONSTRAINT `id`
                 FOREIGN KEY (`teacher_id`)
                 REFERENCES `db`.`teacher` (`id`)
                 ON DELETE NO ACTION
                 ON UPDATE NO ACTION);
        DESC student;
CREATE INDEX idx_name
       ON student (name);
       DESC student;
 ALTER TABLE  db.student
  ADD COLUMN   dob DATE AFTER class ;
      DESC student;
 ALTER TABLE db.student
 ALTER TABLE db.student
CHANGE COLUMN dob age INT CHECK(age<18);
         DESC student;
   DROP TABLE db.student;

