SELECT designation.rank
      ,designation.name
      ,college.name
      ,department.dept_name
      ,university.university_name
  FROM employee 
       RIGHT JOIN designation 
       ON employee.desig_id = designation.id
       RIGHT JOIN college 
       ON college.id = employee.college_id 
       RIGHT JOIN college_department 
       ON employee.cdept_id = college_Department.cdept_id
       RIGHT JOIN department 
       ON college_department.udept_code = department.dept_code
       RIGHT JOIN university 
       ON department.univ_code = university.univ_code
 WHERE employee.name ='' ; 