package com.kpr.training.lambda;

/*
Requirement:To Find which one of these is a valid lambda expression? and why?:
             (int x, int y) -> x+y; or (x, y) -> x + y;
Entities:No entity.
Function Declaration: No function is declared.
Jobs To be Done:Checking the valid expression.


Solution:
    In the given expression, both the expressions are valid expression.
    Because the major difference between two expression is identifier .
    In lambda expression we may or may not include
*/
