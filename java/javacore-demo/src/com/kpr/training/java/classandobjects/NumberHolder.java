/*
Requirement: 
    To create instance and intialize and print the value
    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }

Entity:
     NumberHolder

Function declaration:
     public static void main(String[] args)

Jobs to be done:
    1)anInt and aFloat is declared as integer and float and decalred as public.
    2) An object is created for NumberHolder as numberHolder.
    3)The value is initialized for anInt as 2.
    4)The value is initialized for aFloat as 8.78f.
    5)anInt is printed.
    6)anFloat is printed.
*/

package com.kpr.training.java.classandobjects;

public class NumberHolder {
    
    public int anInt;
    public float aFloat;
    
    public static void main(String[] args) {
        
        NumberHolder numberHolder = new NumberHolder();
        numberHolder.anInt = 2;
        numberHolder.aFloat = 8.78f;
        System.out.println(numberHolder.anInt);
        System.out.println(numberHolder.aFloat);
    }
}