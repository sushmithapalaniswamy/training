/*
Requirement:
	For the following code use the split method() and print in sentence
    String website = "https-www-google-com";
Entity:
	SplitMethodDemo
Function Declaration:
	public static void main(String[] args) {}
Jobs To Be Done:
	1.Declare the variable website of type String and store "https-www-google-com".
	2.split the website where '-' exist and store in string
	  array.
	3.For each word 
	  3.1. Print the word.
Pseudo code:
	public class SplitMethodDemo {
	
	public static void main(String[] args) {
		
			String website = "https-www-google-com";
			String[] array = website.split(-)
			for(String string : array ) {
			     System.out.println(string);
			}
		}
	}
	
 */

package com.kpr.training.regex;

public class SplitMethodDemo {
	
	public static void main(String[] args) {
		
		String website = "https-www-google-com";
		String[] list = website.split("-");
		for (String string : list) {
			System.out.print(string + " ");
		}
	}
}