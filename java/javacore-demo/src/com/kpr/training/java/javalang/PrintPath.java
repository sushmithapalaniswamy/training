/*
Requirement:
    print the absolute path of the .class file of the current class

Entity:
    PrintPath

Function Declaration:
    public static void main(final String[] args)

Jobs To Be Done:
    1)print the absolute path
*/

package com.kpr.training.java.javalang;

public class PrintPath {
    
    public static void main(final String[] args) {
        System.out.println("current path : " +System.getProperty("user.dir" ));
    }
}  