/*
 * Requirement:
 *      Iterate the roster list in Persons class and and print the person without using forLoop/Stream   
 * Entity:
 *      IterateListDemo
 *      Person
 * Function Signature:
 *      public static void main(String[] args)
 * Jobs to be done:
 *      1)Invoke the method createRoaster from Person and store it in roster
 *      2)Invoke the method iterator for roster and store it in iterator of type Person
 *      3)For each person
 *         3.1)Print the name , birthday , gender and emailAddress.
 * 
 * Pseudo code:
 * 
 * public class IterateListDemo {

    public static void main(String[] args) {
        
        List<Person> roster = Person.createRoster();
        Iterator<Person> iterator = roster.iterator();
        while (iterator.hasNext()) {
            Person person = iterator.next();
            System.out.println(person.name + " " + person.birthday + " " + person.gender + " "
                    + person.emailAddress);
        }
    }
 }
 
 */
package com.kpr.training.stream;

import java.util.Iterator;
import java.util.List;

public class IterateListDemo {

    public static void main(String[] args) {
        
    	List<Person> roster = Person.createRoster();
        Iterator<Person> iterator = roster.iterator();
        while (iterator.hasNext()) {
            Person person = iterator.next();
            System.out.println(person.name + "\t" + person.birthday + "\t" + person.gender + "\t"
                    + person.emailAddress);
        }
    }
}