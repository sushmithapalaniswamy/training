SELECT college.code
      ,college.name
      ,college.city
      ,college.state
      ,college.year_opened
      ,department.dept_name
      ,university.university_name
      ,employee.name AS hod_name
  FROM college
       INNER JOIN  college_department 
       ON college.id=college_department.college_id
       INNER JOIN department 
       ON college_department.udept_code=department.dept_code
       INNER JOIN employee 
       ON employee.cdept_id=college_department.cdept_id
       INNER JOIN university
       ON department.univ_code=university.univ_code
 WHERE (department.dept_name='IT'
	    OR department.dept_name='CSE')
        AND employee.desig_id=2
      