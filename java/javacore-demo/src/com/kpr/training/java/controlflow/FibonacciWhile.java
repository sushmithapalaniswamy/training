/*
Requirement:
    To print the fibonacci series using while loop

Entity:
    FibonacciWhile

Function Declaration:
    public static void main(String[] args

Jobs To Be Done:
    1)Declare the variable value1 as int and assign 0 to it.
    2)Declare the variable value2 as int and assign 1 to it.
    3)Declare the variable number as int and assign 0 to it.
    4)Declare the variable sum a int.
    5)Declare the variable limit as int and assign 10 to it.
    6)For each number till the limit
      6.1)Check the number is less than or equal to 1.
          6.1.1)Assign the number to sum.
      6.2)If the condition 6.1 is not satisfied.
          6.2.1)Add value1 and value2 and store it in sum.
          6.2.2)Assign the value2 to value1
          6.2.3)Assign the sum to value2.
      6.3)Print sum
      6.4)Increment the number by 1.
*/

package com.kpr.training.java.controlflow;

class FibonacciWhile {
	
    public static void main(String[] args) {
        
        int value1 = 0 ;
        int value2 = 1 ; 
        int number = 0 ;
        int sum ;
        int limit = 10;
        System.out.print("Fibonacci series:");
        while(number < limit) {
            
            if (number <= 1) { 
                sum = number;
            } else {
             
             sum = value1 + value2;
              value1 = value2;
              value2 = sum;
            }
            System.out.println(sum);
            number++;
        }
    }
}