/*Requirement: Reverse List Using Stack with minimum 7 elements in list.
 *
 * Entity:ReverseList
 * 
 *Function Declaration:public static void main(String[] args)
 *
 *Jobs To Be Done :1)Create a ArrayList named list.
 *                 2)For each number from 1 to 8.
 *                   2.1)Add number to the list.
 *                 3)Print the list.
 *                 4)Create a Stack named stack.
 *                 5)Add all the elements of the list to stack.
 *                 6)Create a ArrayList named reverseList .
 *                 7)For each iteration 
 *                   7.1)Add the peek element to the reverseList.
 *                 8)Print the reverseList.
 *                 
 *pseudo code:
  public class ReverseList {

	 public static void main(String[] args) {
       
	 	 ArrayList<Integer> list = new ArrayList<Integer>();
	 	 
	 	 for(int number = 1; number < 8; number++) {
	 	 	list.add(number);
	 	 }
	 	 
	 	 System.out.println(list);
	 	 
	 	 Stack<Integer> stack = new Stack<Integer>();
	 	 
	 	 stack.addAll(list);
	 	 
	 	 ArrayList<Integer> reverseList = new ArrayList<Integer>();
	 	 
	 	 for(int number = 0; number < 7; number++) {
	 	 	reverseList.add(stack.pop());
	 	 }
	 	 
	 	 System.out.println(reverseList);
	 }
 }
 
 
                  
 */

package com.kpr.training.stack;

import java.util.ArrayList;
import java.util.Stack;

public class ReverseList {

	public static void main(String[] args) {
      
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		for(int number = 1; number < 8; number++) {
			list.add(number);
		}
		
		System.out.println(list);
		
		Stack<Integer> stack = new Stack<Integer>();
		
		stack.addAll(list);
		
		ArrayList<Integer> reverseList = new ArrayList<Integer>();
		
		for(int number = 0; number < 7; number++) {
			reverseList.add(stack.pop());
		}
		
		System.out.println("Reversed list");
		System.out.println(reverseList);
	}
}