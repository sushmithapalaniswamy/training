SELECT student.id
      ,student.name
      ,student.class
      ,mark.social
      ,mark.science
      ,mark.maths
	  ,mark.tamil
      ,mark.english
      ,mark.total
      ,teacher.name
  FROM student  
       CROSS JOIN mark 
       ON student.id = mark.std_id
       CROSS JOIN teacher 
       ON student.teacher_id=teacher.id
  