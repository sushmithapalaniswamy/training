SELECT student.roll_number
      ,student.name
      ,student.gender
      ,student.dob
      ,student.email
      ,student.phone
      ,student.address
      ,college.name
      ,department.dept_name
      ,employee.name AS hod_name
  FROM student
       INNER JOIN college 
       ON college.id = student.college_id
          AND college.city='coimbatore'
          AND college.univ_code='u1'
       INNER JOIN college_department 
       ON student.cdept_id = college_department.cdept_id
       INNER JOIN department  
       ON department.dept_code = college_department.udept_code
       INNER JOIN employee 
       ON employee.cdept_id = college_department.cdept_id
          AND employee.desig_id  = 2