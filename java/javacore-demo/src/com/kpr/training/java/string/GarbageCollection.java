/*Requirement:To analyze the following code and to check the number of reference to those objects exist after
              the code executes.And also checking whether the object is eligible or garbage collection.
              String[] students = new String[10];
              String studentName = "Peter Parker";
              students[0] = studentName;
              studentName = null;
Entity:no entity
Function declartion:There is no function is declared in this program

Jobs to be done:
    1)A String class is declared and its object is students and its array size is 10.
    2)studentName variable is initialized assigned the value as Peter Parker
    3)At Index 0, The studentName is assigned to the array.
    4)Then the studentName is assigned as null.
*/
/*ANSWER:
    There is one reference to the array student.
    It is neither eligible nor garbage collection. 
    The studentName is assigned as null  once the studentName is inserted into the array
     The array size is 10 and its has only one reference value.
*/