/*Requirement: write a Java program reads data from a particular file using FileReader and writes
 *             it to another, using FileWriter
 * 
 * Entity: ReadAndWriteDemo
 * 
 * Function Declaration: public static void main(String[] args);
 * 
 * Jobs to be done: 1. Create a FileWriter named fileWriter to save the destination file path and initialize null.
 * 					2. Create a FileReader named fileReader and initialize to null for the source file path.
 * 					3. While the file has not reached end.
 * 						3.1) Read each characters and write it to destination file.
 * 					4. Close the fleReader.
 *                  5. Close the fileWriter.
 * PseudoCode:
public class ReadAndWriteDemo {
	
	public static void main(String[] args) throws IOException {  
        
        int position; 

    	FileWriter fileWriter = null;
        FileReader fileReader = null; 

        fileReader = new FileReader("C:\\1dev\\training\\java\\javaee-demo\\apple.txt"); 
        fileWriter = new FileWriter("C:\\1dev\\training\\java\\javaee-demo\\write.txt"); 
        while ((position = fileReader.read()) != -1)  {
        	fileWriter.write(position); 
    	}
        fileReader.close(); 
        fileWriter.close();
    } 
}
*/
package com.kpr.training.io;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadAndWriteDemo {
	
	public static void main(String[] args) throws IOException {  
        
		int position; 

    	FileWriter fileWriter = null;
        FileReader fileReader = null; 

        fileReader = new FileReader("C:\\1dev\\training\\java\\javaee-demo\\apple.txt"); 
        fileWriter = new FileWriter("C:\\1dev\\training\\java\\javaee-demo\\write.txt"); 
        while ((position = fileReader.read()) != -1)  {
        	fileWriter.write(position); 
    	}
        fileReader.close(); 
        fileWriter.close();
    } 
}