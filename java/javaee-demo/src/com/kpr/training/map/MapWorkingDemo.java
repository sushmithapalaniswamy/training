/*
 * Requirement:
 *      demonstrate java program to working of map interface using( put(), remove(), 
 *      booleanContainsValue(), replace() )
 * Entity:
 *      MapWorkingDemo
 * Function declaration:
 *      public static void main(String[] args) 
 * Jobs to be done:
 *      1)Create a TreeMap named treemap
 *      2)add elements to the treemap.
 *      3)print the treemap.
 *      4)Remove the key value pair from the tree map.
 *      5)print the treemap.
 *      6)check whether a particular  value is present 
 *        6.1)Print true if it is present.
 *        6.2)Print false if it is not present.
 *      7)Replace particular value.
 *      8)print the treemap
 *      
 * Pseudo code:
		public class MapWorkingDemo {

			public static void main(String[] args) {
				
				//create a new treemap
				//add elements to the treemap
				treemap.remove();
				System.out.println(treemap);
				System.out.println(treemap.containsValue());
				System.out.println(treemap);
				treemap.replace();
				System.out.println(treemap);		
			}
		}
*/
package com.kpr.training.map;

import java.util.TreeMap;

public class MapWorkingDemo {

	public static void main(String[] args) {

		TreeMap<Integer, String> treeMap = new TreeMap<>();
		treeMap.put(1 , "ones");
		treeMap.put(2 , "two");
		treeMap.put(3 , "three");
		treeMap.put(4 , "four");
		treeMap.put(5 , "five");
		System.out.println("treemap:" + treeMap);
		treeMap.remove(1);
		System.out.println("After removing:" + treeMap);
		System.out.println(treeMap.containsValue("three"));
		treeMap.replace(1, "one");
		System.out.println("After replacing the value of 1:" + treeMap);
	}
}
