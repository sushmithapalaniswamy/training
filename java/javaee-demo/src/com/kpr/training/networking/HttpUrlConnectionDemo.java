/*Requirement: To Create a program using HttpUrlconnection in networking.
 * 
 * Entity: HttpsUrlConnectionDemo
 * 
 * Method Signature: public static void main(String[] args);
 * 
 * Jobs to be done: 1. Create a url.
 * 					2. Create HttpUrlConnection using the created url.
 * 					3. Find the information of url header and print it.
 * PseudoCode:

public class HttpUrlConnectionDemo {
	
	public static void main(String[] args) throws IOException {    
		
		URL url = new URL("http://www.javatpoint.com/java-tutorial");    
		HttpURLConnection huc = (HttpURLConnection)url.openConnection();  
		for(int i = 1 ;i <= 8 ;i++) {  
			System.out.println(huc.getHeaderFieldKey(i) + " = " + huc.getHeaderField(i));  
		}  
		huc.disconnect(); 
	}
}

 */
package com.kpr.training.networking;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUrlConnectionDemo {
	
	public static void main(String[] args) throws IOException {    
		
		URL url = new URL("http://www.javatpoint.com/java-tutorial");    
		HttpURLConnection huc = (HttpURLConnection)url.openConnection();  
		for(int i = 1 ;i <= 8 ;i++) {  
			System.out.println(huc.getHeaderFieldKey(i) + " = " + huc.getHeaderField(i));  
		}  
		huc.disconnect(); 
	}
}
