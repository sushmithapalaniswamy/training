/*Requirement:
 *      8 districts are shown below
 *      Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy   to be converted to UPPERCASE.
 *
 *Entity:
 *     ConvertToUpperCase
 *
 *Function Declaration:
 *     public static void main(String[] args)
 *
 *Jobs to be done:
 *     1)Create a ArrayList.
 *     2)Add the cities to the list.
 *     3)For each city in the list
 *       3.1)Convert the city into uppercase.
 *     4)Print the list.
 *     
 *Pseudo code:
 *  
 *  public class ConvertToUpperCaseDemo {
 *   
 *      public static void main(String[] args) {
 *      
 *      	List<String> list = new ArrayList<>();
            //add the elements 
            list.replaceAll(city -> city.toUpperCase());
            System.out.println(list);
 *      }
 * }      
 */
package com.kpr.training.list;

import java.util.ArrayList;
import java.util.List;

public class ConvertToUpperCase {
	
	public static void main(String[] args) {
    	
        List<String> list = new ArrayList<>();
        list.add("Madurai");
        list.add("Coimbatore");
        list.add("Theni");
        list.add("Chennai");
        list.add("Karur");
        list.add("Salem");
        list.add("Erode");
        list.add("Trichy");
        
        list.replaceAll(city -> city.toUpperCase());
        System.out.println("After converting to uppercase:" + list);
    }

}
