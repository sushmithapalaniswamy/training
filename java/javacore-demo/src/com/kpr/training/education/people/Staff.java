package com.kpr.training.education.people;

import java.util.Scanner;

import com.kpr.training.education.college.College;

public class Staff extends People {
	
	private String qualification ;
	
	public static void main(String[] args) {
		
		Staff staff = new Staff();
		College college = new College();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the name:");
		String name = scanner.nextLine(); //protected access in people
		System.out.println("Enter the id:");
		int id = scanner.nextInt();//public in people
		System.out.println("Enter the Qualification:");
		String qualification = scanner.nextLine();
		staff.defaultMethod(name , id) ;//default in people
		//student.printDetailsname , id) ;//private in people //error
		System.out.println("Qualification:" + staff.qualification);
		System.out.println("college name:" + college.collegeName);
		scanner.close();		
	}
}
