/*
 * Requirements : 
 *      Write a generic method to count the number of elements in a collection that have a specific
 * property (for example, odd integers, prime numbers, palindromes).
 * Entity :
 *      CountOddIntegerDemo
 * Function Declaration :
 *      public static void main(String[] args)
 * Jobs To Be Done:
 *      1)Declare a List named list .
 *      2)Add elements to the list.
 *      3)Invoke the method count and pass list to it.
 *        3.1)Declare a variable named total of type int and store 0 in it.
 *        3.2)For each element in the list.
 *            3.2.1)Check whether the number is odd.
 *            3.2.2)If the number is odd , increment the total by one.
 *        3.3)Return the total
 *      4)Print the retured value.
 *      
 * Pseudo Code:
public class CountOddIntegerDemo {
    
    public static int count(ArrayList<Integer> list) {
    	
        int total = 0;
        for(int number : list) {
            if(number % 2 != 0) {
                total++;
            }
        }
        return total;
    }

    public static void main(String[] args) {
    	
        ArrayList<Integer> list = new ArrayList<>();
        //Add elements to the list
        System.out.println("Number of odd integers : " + count(list));
    }
}
 */
package com.kpr.training.generics;

import java.util.ArrayList;

public class CountOddIntegerDemo {
    
    public static int count(ArrayList<Integer> list) {
    	
        int total = 0;
        for(int number : list) {
            if(number % 2 != 0) {
                total++;
            }
        }
        return total;
    }

    public static void main(String[] args) {
    	
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        System.out.println("Number of odd integers : " + count(list));
    }
}