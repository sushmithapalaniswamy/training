/*Requirement:
 *      Consider the following Person:
 *          new Person(
 *              "Bob",
 *              IsoChronology.INSTANCE.date(2000, 9, 12),
 *              Person.Sex.MALE, "bob@example.com"));
 *     - Check if the above person is in the roster list obtained from Person class.
 * 
 * Entity:
 *      CheckRoster
 * 
 * Method Signature:
 *      public static void main(String[] args)
 * 
 * Jobs To Be Done:
 *      1) Declare the boolean variable named flag and store false in it.
 *      2) Invoke the method createRoaster from Person and store it in roster.
 *      3) Create an object for the Person named newPerson and store the given person in it.
 *      4) Checks Whether the person is present or not
 *           5.1) If the given person is present in Person then change the present value is true.
 *      6) Checks Whether present value is true or false 
 *           6.1) If the present value is equals to true, print " Person is present"
 *           6.2) Otherwise print " Person is not present".
 * Pseudo Code:
 * 
 *  class CheckRoster {

    public static boolean present = false;

    public static void main(String[] args) {
     
        List<Person> roster = Person.createRoster();
        
        Person newPerson = new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");
         stream.forEach(person -> {
         if ((person.getName().equals(newPerson.getName()))
            && (person.getBirthday().equals(newPerson.getBirthday()))
            && (person.getEmailAddress().equals(newPerson.getEmailAddress()))
            && (person.getGender().equals(newPerson.getGender()))) {
                present = true;
            }
        });
        if (present == true) {
            System.out.println("Person is present");
        } else {
            System.out.println("Person is not present");
        }
}

*/
package com.kpr.training.stream;

import java.time.chrono.IsoChronology;
import java.util.List;
import java.util.stream.Stream;

public class CheckRoster {

    public static boolean flag = false;

    public static void main(String[] args) {
        
    	List<Person> roster = Person.createRoster();
        Person newPerson = new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> {
            if ((person.getName().equals(newPerson.getName()))
                && (person.getBirthday().equals(newPerson.getBirthday()))
                && (person.getEmailAddress().equals(newPerson.getEmailAddress()))
                && (person.getGender().equals(newPerson.getGender()))) {
                    flag = true;
            }
        });
        if (flag == true) {
            System.out.println("Person is present");
        } else {
            System.out.println("Person is not present");
        }

    }
}