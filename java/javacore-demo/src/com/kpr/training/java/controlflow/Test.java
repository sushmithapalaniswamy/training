/*Requirement:
    To find the output of the given program if the value of aNumber is 3.
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

Entities:
    Test

Function Declaration:
    public static void main (String[] args)

*/
/*OUTPUT:
third string
*/

package com.kpr.training.java.controlflow;

class Test {
    
    public static void main (String[] args) {
        
        int aNumber = 3;
        if (aNumber >= 0) {
            
            if (aNumber == 0) {
                
                System.out.println("first string");
            }
        } else {
            
                System.out.println("second string");
        }
    System.out.println("third string");
    }
}
