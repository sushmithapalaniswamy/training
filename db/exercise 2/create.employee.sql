CREATE TABLE `db_2`.`employee` (
             `id` INT NOT NULL
            ,`first_name` VARCHAR(45) NOT NULL
            ,`surname` VARCHAR(45) NULL
            ,`dob` DATE NOT NULL
			,`date_of_joining` DATE NOT NULL
			,`annual_salary` INT NOT NULL
			,`dept_id` INT NULL
			,PRIMARY KEY (`id`)
			,INDEX `id_idx` (`dept_id` ASC) VISIBLE
			,CONSTRAINT `id`
                 FOREIGN KEY (`dept_id`)
                 REFERENCES `db_2`.`department` (`id`)
                 ON DELETE NO ACTION
                 ON UPDATE NO ACTION);
