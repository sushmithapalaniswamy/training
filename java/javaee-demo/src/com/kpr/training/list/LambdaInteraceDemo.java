/*Requirement:
    Addition,Substraction,Multiplication and Division concepts are achieved using 
    Lambda expression and functional interface.

Entity:
    LambdaInterfaceDemo

Function Declaration:
    public static void main(String[] args){} 

Jobs To Be Done:
    1)Create an object for the Scanner as scanner.
    2)Get the input from the user and store it in firstNumber.
    3)Get the input from the user and store it in secondNumber.
    4)Add the two input using lambda expression
    5)Subtract the two input using lambda expression
    6)Multiply the two input using lambda expression
    7)Divide the two input usinh lambda expression.
      7.1)Throw an exception when the division cannot be performed for the given inputs 
      
Pseudo code:
interface ArithmeticOperation {
	
	int values(int firstumber , int secondNumber);
}

public class LambdaInteraceDemo {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
	
		int firstNumber = scanner.nextInt();
		
		int secondNumber = scanner.nextInt();
		
		
		ArithmeticOperation addition = (int a, int b) -> (a + b);
        System.out.println("Addition = " + addition.values(firstNumber, secondNumber));

        ArithmeticOperation subtraction = (int a, int b) -> (a - b);
        System.out.println("Subtraction = " + subtraction.values(firstNumber, secondNumber));

        ArithmeticOperation multiplication = (int a, int b) -> (a * b);
        System.out.println("Multiplication = " + multiplication.values(firstNumber, secondNumber));
        
        try {
        	
        	ArithmeticOperation division = (int a, int b) -> (a / b);
            System.out.println("Division = " + division.values(firstNumber, secondNumber));
        	
        } catch (Exception exception) {
        	System.out.println("correct values must be inserted to perform division");
        }
	}
}
*/

package com.kpr.training.list;

import java.util.Scanner;

interface ArithmeticOperation {
	
	int values(int firstumber , int secondNumber);
}

public class LambdaInteraceDemo {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Enter the First Number : " );
		int firstNumber = scanner.nextInt();
		
		System.out.print("Enter the Second Number : ");
		int secondNumber = scanner.nextInt();
		
		
		ArithmeticOperation addition = (int a, int b) -> (a + b);
        System.out.println("Addition = " + addition.values(firstNumber, secondNumber));

        ArithmeticOperation subtraction = (int a, int b) -> (a - b);
        System.out.println("Subtraction = " + subtraction.values(firstNumber, secondNumber));

        ArithmeticOperation multiplication = (int a, int b) -> (a * b);
        System.out.println("Multiplication = " + multiplication.values(firstNumber, secondNumber));
        
        try {
        	
        	ArithmeticOperation division = (int a, int b) -> (a / b);
            System.out.println("Division = " + division.values(firstNumber, secondNumber));
        	
        } catch (Exception exception) {
        	System.out.println("correct values must be inserted to perform division");
        }
      
		scanner.close();
	}
}