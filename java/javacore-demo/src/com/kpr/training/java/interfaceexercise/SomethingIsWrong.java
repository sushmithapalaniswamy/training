/*
Requirement:What is wrong with the following interface? and fix it.
            public interface SomethingIsWrong {
                void aMethod(int aValue){
                    System.out.println("Hi Mom");
                }
            }
Entity:
    SomethingIsWrong

Function Declaration:
    void aMethod(int aValue)

Jobs To be Done:
    1)Identify the error
    2)Write the corrected program


ANSWER:interface can have only signature of the method but the above program contains implemetion of 
       the method 
*/

package com.kpr.training.java.interfaceexercise;

public interface SomethingIsWrong {
    void aMethod(int aValue);     // Here default or static can be used.
}