/*
Requirement:
    To Write a class that implements the CharSequence interface found in the java.lang package.
    And the implementation should return the string backwards.

Entity:
    CharSeqDemo.
    CharSequence.

Function Declaration:
    toCharArray()
    clone().
    length()
    subSequence()
    toString()
    charAt()
    public static void main(String[] args)

Jobs to be Done:
    1) Declare a varaiable string of type String and store hello wild in it.
    2) Invoke the constructor charSeqDemo with the parameter string and store it in word.
      2.1)Covert  the string as character array and store in array.
      2.2)Another copy of the array is stored in reverseArray.
      2.3)Subtract the array length by one and store it in j.
      2.4)for each number in the array
          2.4.1)Store the number to the jth index of reversedArray
          2.4.2)Decrement j by one.
      2.5)Convert the reversedArray to string and store in string.
      2.6)print string which is the reversed string
    3)Invoke the method length from charSeqDemo
      3.1)Returns the length of the word and it is printed.
    4)Invoke the method subSequence from charSeqDemo
      4.1)Returns the subsequence  of the word and it is printed.
    5)Invoke the method charAt from charSeqDemo
      5.1)Returns the character at the specified index  of the word and it is printed.
    6)Invoke the method toString 
      6.1)Returns the string and the string is printed.  
      
    
*/

package com.kpr.training.java.interfaceexercise;

import java.lang.CharSequence ;

public class CharSequenceDemo  implements CharSequence {
    
    public String string;
     
    public  CharSequenceDemo(String string) {
        
        char[] array = string.toCharArray();
        char[] reverseArray = array.clone();
        int j = array.length - 1;
        for (int i = 0; i < array.length; i++) {
            
            reverseArray[j] = array[i];
            j--;
        }
        this.string = new String(reverseArray);
        System.out.println("The reversed string is " + this.string);
    }
    
    public int length() {
        return string.length();
    }
    
    public CharSequence subSequence(int i , int j) {
        return string.subSequence(i,j);
    }
    
    public char charAt(int i) {
        return string.charAt(2);
    }    
    
    public String toString() {
        return string;
    }
 
    public static void main (String[] args) {
    
        String string = "HELLO WILD" ;
        CharSequenceDemo word = new CharSequenceDemo(string);
        System.out.println("The lenth of the string is " + word.length());
        System.out.println("the substring is " + word.subSequence(1,3));
        System.out.println("The character at 2nd index:" + word.charAt(2));
        System.out.println("The string is:" + word.toString());
    }
}