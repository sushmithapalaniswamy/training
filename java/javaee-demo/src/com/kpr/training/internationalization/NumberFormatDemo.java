/*Requirement: To change the number format to Denmark number format .

 * Entity:NumberFormatDemo
 * 
 * Function Declaration : public static void main(String[] args);
 * 
 * Jobs To be Done: 1.Initialise a number to number variable.
 * 					2. Find the number format for Denmark.
 * 					3. Convert the number to denmark number format.
 * 					4. Print the converted format in console.
 * 
 * PseudoCode:

public class NumberFormatDemo {
	
	public static void main(String[] args) {
		        
		double number = 1240.35;
		//Find the number format for denmark.
		System.out.println(value);
	}
}

 */
package com.kpr.training.internationalization;

import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatDemo {
	
	public static void main(String[] args) {
		        
		double number = 12.35;

		NumberFormat numberFormat = NumberFormat.getInstance(new Locale("da", "DK"));
		String value = numberFormat.format(number);
		System.out.println(value);
	}
}