SELECT employee.id
     ,employee.name
     ,employee.dob
     ,employee.email
     ,employee.phone
     ,college.name
     ,department.dept_name
 FROM employee
      INNER JOIN college 
      ON college.id=employee.college_id
      INNER JOIN college_department 
      ON college_department.cdept_id = employee.cdept_id
      INNER JOIN department  
      ON department.dept_code = college_department.udept_code
         AND department.univ_code='u1'
ORDER BY college.name , employee.desig_id