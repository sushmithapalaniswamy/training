/*Requirement:Consider a following code snippet
                Queue bike = new PriorityQueue();    
                bike.poll();
                System.out.println(bike.peek());    

           what will be output and complete the code.
 * 
 * Entity:BikeQueue
 * 
 * Function Declaration:public static void main(String[] args)
 * 
 * Output : null
 */

package com.kpr.training.queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class BikeQueue {

	public static void main(String[] args) {
	
	Queue bike = new PriorityQueue();    
    bike.poll();
    System.out.println(bike.peek());    
    }
}
