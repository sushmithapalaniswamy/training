/*
 Requirement :
   create a pattern for password which contains
   8 to 15 characters in length
   Must have at least one upper case letter
   Must have at least one lower case letter
   Must have at least one digit

Entity:
    PasswordDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1)Create a Scanner named scanner
    2)Get the password as input from the user and store it in password
    3)Invoke the method validatePassword and store it in result.
      3.1)Create a regex pattern for password and store it in pattern of type Pattern.
      3.2)Create a Matcher named matcher and it matches the password  with pattern
      3.3)If the pattern matches it will return true otherwise it will return false
    4)Check whether the result is true
      4.1)Print "password is valid"
      4.2)Otherwise print "password is invalid"

pseudo code:
public class PasswordDemo {
	
    public static boolean validPassword(String password) {
     
        Create a regex pattern for password and store it in pattern of type Pattern.
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String password = scanner.next();
        boolean result = validPassword(password);
        if (result) {
            System.out.print("password is valid");
        } else {
            System.out.print("Password is invalid");
        }
        scanner.close();
    }
}  
 */
package com.kpr.training.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordDemo {
	
    public static boolean validPassword(String password) {
     
        Pattern pattern = Pattern.compile("^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + ".{8,12}$");
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static void main(String[] args) {
        
    	Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the Password ");
        String password = scanner.next();
        boolean result = validPassword(password);
        if (result) {
            System.out.print("password is valid");
        } else {
            System.out.print("Password is invalid");
        }
        scanner.close();
    }
}