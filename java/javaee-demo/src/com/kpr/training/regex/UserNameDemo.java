/*
 Requirement :
   create a username for login website which contains
   8-12 characters in length
   Must have at least one uppercase letter
   Must have at least one lower case letter
   Must have at least one digit


Entity:
    UserNameDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1)Create a Scanner named scanner
    2)Get the userName as input from the user and store it in userName
    3)Invoke the method validateUserName and store it in result.
      3.1)Create a regex pattern for userName and store it in pattern of type Pattern.
      3.2)Create a Matcher named matcher and it matches the userName  with pattern
      3.3)If the pattern matches it will return true otherwise it will return false
    4)Check whether the result is true
      4.1)Print "username is valid"
      4.2)Otherwise print "username is invalid"

pseudo code:
public class UserNameDemo {
	
    public static boolean validPassword(String userName) {
     
        Create a regex pattern for userName and store it in pattern of type Pattern.
        Matcher matcher = pattern.matcher(userName);
        return matcher.matches();
    }

    public static void main(String[] args) {
       
        Scanner scanner = new Scanner(System.in);
        String userName = scanner.next();
        boolean result = validPassword(userName);
        if (result) {
            System.out.print("username is valid");
        } else {
            System.out.print("username is invalid");
        }
        scanner.close();
    }
}  
 */
package com.kpr.training.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserNameDemo {
	
    public static boolean validateUserName(String userName) {
     
        Pattern pattern = Pattern.compile("^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + ".{8,12}$");
        Matcher matcher = pattern.matcher(userName);
        return matcher.matches();
    }

    public static void main(String[] args) {
        
    	Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the username ");
        String userName = scanner.next();
        boolean result = validateUserName(userName);
        if (result) {
            System.out.print("username is valid");
        } else {
            System.out.print("username is invalid");
        }
        scanner.close();
    }
}