/*
Requirement:
    To demonstrate object equality using Object.equals() vs ==, using String objects

Entity:
    EqualDemo

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1)Create a object string of type String. 
    2)Create a object string1 of type String. 
    3)Check whether string and string1 are equal using == operator 
      3.1)If the condition is satisfied print true
      3.2)Otherwise print as false.
    4)Check whether string and string1 are equal using equals method print true
       4.1)If the condition is satisfied print true
       4.2)Otherwise print as false

*/

package com.kpr.training.java.javalang;

public class EqualDemo { 

    public static void main(String[] args) { 
        
        String string = new String("wild"); 
        String string1 = new String("wild"); 
        if(string == string1) {    
            System.out.println("True");
        } else {
           System.out.println("False");
        }
        if(string.equals(string1)) {    
            System.out.println("True");
        } else {   
            System.out.println("False");
        }
    } 
}