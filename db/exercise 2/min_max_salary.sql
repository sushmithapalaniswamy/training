SELECT department.name 
      ,min(annual_salary) AS min_salary
	  ,max(annual_salary) AS max_salary
  FROM db_2.employee 
      ,db_2.department
 WHERE department.id = employee.dept_id
 GROUP BY Dept_id