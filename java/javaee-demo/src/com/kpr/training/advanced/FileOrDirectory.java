/*
 * Requirements:
 *     To check the given path is file or directory.
 *    
 * Entities:
 *     FileOrDirectory
 *     
 * Function Declaration:
 *     public static void main(String[] args);
 *     
 * Jobs To Done:
 *     1.Get the file or directory path.
 *     2.Check whether the given path is file or directory.
 *     3.if it is file 
 *         3.1)Print this is file
 *
 * Pseudo code:
 *     class FileOrDirectory {
 *
 *         public static void main(String[] args) {
 *             
 *             File file = new File("C:\\1dev\\training\\java\\javaee-demo\\src\\com\\kpr\\training\\nio");
 *	           if (file.isFile()) {
 *	        	   System.out.println("This is file");
 *	           } else {
 *	        	   System.out.println("This is directory");
 *	           }  
 *         }
 *     }   
 */

package com.kpr.training.advanced;

import java.io.File;

public class FileOrDirectory {

	public static void main(String[] args) {
		
		File file = new File("C:\\1dev\\training\\java\\javaee-demo\\src\\com\\kpr\\training\\nio");
		if (file.isFile()) {
			System.out.println("This is file");
		} else {
			System.out.println("This is directory");
		}
	}
}
