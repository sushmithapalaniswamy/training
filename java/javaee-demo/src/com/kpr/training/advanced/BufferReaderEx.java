/*
 * Requirements : 
 * 		Read a any text file using BufferedReader and print the content of the file
 * Entity :
 * 		BufferedReaderEx.
 * Function Declaration :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1.Create a reference for BufferedReader wrapped with FileReader having file as constructor argument.
 *     	2.Till the end of the file
 *          2.1)Read the content of the file.
 *          2.2)Print the content of the file.
 *     	3.Close the created input stream.
 * PseudoCode:
 * 
 class BufferReaderEx {
    
    public static void main(String[] args) throws Exception {
        
    	Reader reader = new FileReader("ReaderEx.txt");
        BufferedReader bufferReader = new BufferedReader(reader);
        int charater;
        
        while ((charater = bufferReader.read()) != -1) {
            System.out.print((char) charater);
        }
        
        bufferReader.close();
    }
}
*/
package com.kpr.training.advanced;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;

public class BufferReaderEx {
    
    public static void main(String[] args) throws Exception {
        
    	Reader reader = new FileReader("ReaderEx.txt");
        BufferedReader bufferReader = new BufferedReader(reader);
        int charater;
        
        while ((charater = bufferReader.read()) != -1) {
            System.out.print((char) charater);
        }
        bufferReader.close();
    }
}
