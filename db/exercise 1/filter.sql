
SELECT id 
       , name 
	   , teacher_id 
	   , age
  FROM db.student
 WHERE age = '15' OR teacher_id = '1';
SELECT id 
	   , name
	   , teacher_id
	   , age
  FROM db.student
 WHERE class = '10' AND teacher_id = '3';
SELECT id 
      ,name 
	  ,teacher_id 
	  ,age
  FROM db.student
 WHERE name IN ('shibi', 'kavi');
SELECT id 
      ,name
	  ,teacher_id 
	  ,age
  FROM db.student
 WHERE NOT teacher_id = '3';
SELECT id 
      ,name 
	  ,teacher_id 
	  ,age
  FROM db.student
 WHERE name LIKE 's%';
SELECT id 
      ,name 
	  ,teacher_id 
	  ,age
  FROM db.student
 WHERE age LIKE '%5';
SELECT id 
      ,name 
	  ,teacher_id 
	  ,age
  FROM db.student
 WHERE name LIKE 'r_a' ;
SELECT name 
  FROM student
 WHERE teacher_id = ANY 
                   (SELECT id FROM teacher WHERE section = 'D' );