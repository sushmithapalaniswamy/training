/*
Requirement:
    Find the value of expression
    What is the value of the following expression, and why?
     Integer.valueOf(1).equals(Long.valueOf(1))

Solution:

    Value of the expression - false
    Both values are same but differ in their data type.
*/