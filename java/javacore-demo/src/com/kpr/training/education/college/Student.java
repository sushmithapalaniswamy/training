package com.kpr.training.education.college;

import java.util.Scanner;

import com.kpr.training.education.people.People;
import com.kpr.training.education.university.University;

public class Student extends People {
	
	public int year;
	
	public static void main(String[] args) {
		
		Student student = new Student();
		College college = new College();
		University university = new University();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the name:");
		String name = scanner.nextLine(); //protected access in people (subclass so it can be accessed)
		System.out.println("Enter the id:");
		int id = scanner.nextInt();//public in people
		//student.defaultMethod(name , id) ;//default in people //error
		//student.printDetailsname , id) ;//private in people //error
		System.out.println("University name:" + university.universityName);
		System.out.println("college name:" + college.collegeName);
		System.out.println("name:" + name);
	    System.out.println("id:" +id);
		scanner.close();	
		
	}

}
