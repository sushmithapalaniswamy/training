/*Requirement:
 *    Java program to demonstrate adding elements, displaying, removing, and iterating in
 *    hash set
 * 
 * Entity:
 *    HashSetDemo
 * 
 * Function Declaration:
 *    public static void main(String[] args)
 * 
 * Jobs To Be Done:
 *    1)Create a HashSet named hashset 
 *    2)Add the elements to the hashset.
 *    3)Print the hashset
 *    4)Remove a particular element in the hashset.
 *    5)For each element in hashset
 *      5.1)Print the element.
 *    
 *    
 * pseudo code:
 *  public class HashSettDemo {
 *     
 *     public static void main(String[] args) {
 *     		
 *     		HashSet<Integer> hashset = new HashSet<Integer> ;
 *          
 *          for(number = 0 ;number <= 10 ;number++) {
 *                add element to the hashset
 *          }
 *          System.out.println("hashset":hashset);
 *          Remove an element in the hashset
 *         
 *          for(number = 0 ; number <= 5 ; number++) {
 *                  hashset.add(number);
 *          }
 *     }
 * }
 */
package com.kpr.training.set;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetDemo {
	
	public static void main(String[] args) {
		
		HashSet<Integer> hashSet = new HashSet<Integer>() ;
		          
		for(int number = 0 ;number <= 10 ;number++) {
            hashSet.add(number) ;
        }
		         
		//display
		System.out.println("hashset:" + hashSet);
		          
		//remove
		hashSet.remove(5);
		System.out.println("After Removing:" + hashSet);

		//Iterating
		Iterator<Integer> iterator = hashSet.iterator();
		while(iterator.hasNext()) {

			Integer element = iterator.next();
			System.out.println(element);
		}	
	}

}
