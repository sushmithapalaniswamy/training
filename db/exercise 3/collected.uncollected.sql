SELECT semester
      ,college.name
      ,university.university_name
      ,SUM(amount) AS collected
  FROM semester_fee 
       INNER JOIN student 
       ON student.id = semester_fee.stud_id 
       INNER JOIN college 
       ON college.id = student.college_id
       INNER JOIN university 
       ON university.univ_code = college.univ_code and semester_fee.paid_status = 'paid' 
 GROUP BY college.name
       ,semester_fee.semester;
SELECT semester
      ,college.name
      ,university.university_name
	,SUM(amount) AS uncollected
  FROM semester_fee 
       INNER JOIN student 
       ON student.id = semester_fee.stud_id 
       INNER JOIN college 
       ON college.id = student.college_id
       INNER JOIN university 
       ON university.univ_code = college.univ_code  
          AND semester_fee.paid_status = 'unpaid'
  GROUP BY college.name,semester_fee.semester;