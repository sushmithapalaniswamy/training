/*Requirement:+ Create a set
                => Add 10 values
                => Perform addAll() and removeAll() to the set.
                => Iterate the set using 
                  - Iterator
                  - for-each
             + Explain the working of contains(), isEmpty() and give example.
 * Entities:SetDemo
 * Function Declaration:public static void main(String[] args)
 * Jobs To Be Done:1.consider the program
 *                 2.A set is created named set
 *                 3.Values are added into the set.
 *                 4.Another set is created named set1
 *                 5.values of set is added to set1 
 *                 6.set1 is printed
 *                 7.The values in the set1 is removed if the value 
 *                   is present in set 
 *                 8.Print the set.
 *                 9.Iterate the elements in the set
 *                   9.1.print the element.
 *                 10.For each element
 *                   10.1.Print the element.
 *                 11.Check whether the specified element is present
 *                    11.1.If it is present print as present
 *                    11.2.Otherwise print as not present.
 *                 12.Check whether the set is empty.
 *                    12.1.Print as empty
 *                    12.2.Otherwise print as not empty.
 *pseudo code:
 public class SetDemo {
	
	public static void main(String[] args) {
		
		Set<Integer> set = new HashSet<Integer>(); 
		
		//add elements to the set
		
		Set<Integer> set1 = new HashSet<Integer>();
		
		//add elements from the set to set1
		System.out.println(set1);
		
		//remove all the elements from the set1
		System.out.println(set1);
		
		Iterator number = set.iterator();
		while(number.hasNext()) {
			System.out.println(number.next());
		}
		
		System.out.println("Iterating list using for each");
		for(int digit : set) {
			System.out.println(digit) ;
		}
		
		
		if(set.contains(1)) {
			System.out.println("1 is present");
		} else {
			System.out.println("1 is not present");
		}
		
		
		if(set1.isEmpty()) {
			System.out.println("set is empty");
		} else {
			System.out.println("set is not empty");
		}	
	}
}                
 */

package com.kpr.training.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {
	
	public static void main(String[] args) {
		
		Set<Integer> set = new HashSet<Integer>(); 
		
		//adding elements to the set
		for(int number = 0 ;number < 11 ;number++) {
			set.add(number+10);
		}
		
		Set<Integer> set1 = new HashSet<Integer>(); 
		
		//addall
		set1.addAll(set);
		System.out.println(set1);
		
		//removeall
		set1.removeAll(set);
		System.out.println(set);
		
		//using Iterator
		System.out.println("Iterating list using iterator");
		Iterator number = set.iterator();
		while(number.hasNext()) {
			System.out.println(number.next());
		}
		
		//iterate using for each loop
		System.out.println("Iterating list using for each");
		for(int digit : set) {
			System.out.println(digit) ;
		}
		
		//contains
		if(set.contains(1)) {
			System.out.println("1 is present");
		} else {
			System.out.println("1 is not present");
		}
		
		//isempty
		if(set1.isEmpty()) {
			System.out.println("set is empty");
		} else {
			System.out.println("set is not empty");
		}
	}
}
