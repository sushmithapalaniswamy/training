package com.kpr.training.generics;

/*
 * Requirements : 
 *      Will the following class compile? If not, why?
 *      public final class Algorithm {
 *          public static <T> T max(T x, T y) {
 *              return x > y ? x : y;
 *          }
 *      }
 *
 * Entities :
 *      public class Algorithm.
 * Function Declaration :
 *      public static <T> T max(T x, T y)
 */

/*

public class Algorithm {

    public static <T> T max(T x, T y) {
        return (x > y) ? x : y;
    }

}
*/

/*
 * The code does not compile, because the greater than (>) operator applies only to primitive numeric types.
 */