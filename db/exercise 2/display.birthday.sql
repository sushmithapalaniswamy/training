SELECT employee.id
      ,employee.first_name
      ,employee.surname
	  ,employee.dob
	  ,employee.date_of_joining
	  ,employee.annual_salary
  FROM  employee 
 WHERE DATE_FORMAT(dob,'%d-%m') = DATE_FORMAT(SYSDATE(),'%d-%m');