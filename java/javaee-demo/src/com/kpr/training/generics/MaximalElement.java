/* 
Requirement:
    To find the maximum element in the range [begin, end) of a list.


Entity:
    MaximalElement

Function Declaration:
    public static void main(String[] args)
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end)
    
Jobs to be Done:
    1. Declare a method public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end)
    2. Compare the elements and find the maximum between the range and return it
    3. Under the main method create an object list or List<Integer>
    4. Call the method max() and print the result.
    
pseudo code:

	public class  MaximalElement {
    
    	public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end) {
        	T maximumElement = list.get(begin);
        	for (++begin; begin < end; ++begin) {
            	if (maximumElement.compareTo(list.get(begin)) < 0) {
                	maximumElement = list.get(begin);
            	}
        	}   
        	return maximumElement;
    	}
    
    	public static void main(String[] args) {
    	
        	List<Integer> list = new LinkedList<>();
        	//add the elements to the list
        	System.out.println(max(list, 0, list.size()));
    	}
	}
*/

package com.kpr.training.generics;

import java.util.LinkedList;
import java.util.List;

public class  MaximalElement {
    
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end) {
        T maximumElement = list.get(begin);
        for (++begin; begin < end; ++begin) {
            if (maximumElement.compareTo(list.get(begin)) < 0) {
                maximumElement = list.get(begin);
            }
        }   
        return maximumElement;
    }
    
    public static void main(String[] args) {
    	
        List<Integer> list = new LinkedList<>();
        
        list.add(11);
        list.add(21);
        list.add(33);
        list.add(45);
        list.add(51);
        list.add(26);
        list.add(77);
        list.add(10);
        list.add(80);
        list.add(100);
        
        System.out.println(max(list, 0, list.size()));
    }
}