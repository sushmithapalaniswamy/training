/* Requirement:to print the volume of  a Rectangle using lambda expression
   Entity:VolumeOfRectangle
   Function declaration:public static void main(String[] args)
	                    public int volume(int length,int breath,int height)
   Jobs to be done:1.Consider the program
 *                 2.An interface is created named LamdaInterfaceDifference
 *                 3.Inside the interface , single method is declared named volume
 *                 4.Inside the main method , create lambda expression 
 *           	     with three parameters which returns volume of rectangle.
 * 				   5.Print the volume of the rectangle.
 * Pseudo code:
  interface LambdaRectangle {
		public int volume(int length,int breath,int height);
  }

  public class VolumeOfRectangle {

	public static void main(String[] args) {
			
			//lambda expression which returns volume.
			System.out.print("volume of rectangle:");
			System.out.println(lambdaRectangle.volume(10, 8, 10));
			
    }
}

 
 */
package com.kpr.training.lambda;

interface LambdaRectangle {
	
	public int volume(int length,int breath,int height);
}

public class VolumeOfRectangle {

	public static void main(String[] args) {
			
			LambdaRectangle lambdaRectangle = (length , breath , height) -> {
			     
				return length * breath * height ;
			};
			
			System.out.print("volume of rectangle:");
			System.out.println(lambdaRectangle.volume(10, 8, 10));
			
    }
}


