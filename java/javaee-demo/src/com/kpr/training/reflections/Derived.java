/*
 * Requirement: To USE OVERRIDE ANNOTATION and complete the program.
				SHOW THE OUTPUT OF THIS PROGRAM: 
class Base 
{ 
     public void display() 
     { 
         
     } 
} 
class Derived extends Base 
{ 
     public void display(int x) 
     { 
         
     } 
  
     public static void main(String args[]) 
     { 
         Derived obj = new Derived(); 
         obj.display(); 
     } 
}

Entity: Base, Derived

Method Signature: public void display(int x);
 				  public void display(int x);
 				  public static void main(String args[]);
 				
Jobs to be done: 1. Create an object for the extended Derived class.
				 2. Invoke the object's display method with integer parameter passed.
				 3. The method display of Derived class is invoked as display() is 
				    overridden and is executed.
				 4. The number passed as parameter is printed.
				 
 */
package com.kpr.training.reflections;

class Base { 
     
	public void display(int x) { 
         System.out.println(x);
     } 
} 

public class Derived extends Base { 
	
	@Override
     public void display(int x) { 
		System.out.println("Number is: " + x );
     } 
  
     public static void main(String args[]) { 
         
    	 Derived obj = new Derived(); 
         obj.display(5); 
     } 
}