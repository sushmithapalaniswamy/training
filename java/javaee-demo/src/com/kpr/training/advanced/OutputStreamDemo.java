/*
 * Requirements:
 *     To write some String content using OutputStream
 *    
 * Entities:
 *     OutputStreamDemo
 *     
 * Function Declaration:
 *     public static void main(String[] args);
 *     
 * Jobs To Done:
 *     1.Create a reference for FileOutputStream with file as constructor argument.
 *     2.Get the content to be wrote in the file.
 *     3.Convert the given string into input stream 
 *     4.Write the converted content to a file
 *     5.Close the created output stream.
 *
 * Pseudo code:
 *     class OutputStreamDemo {
 *
 *         public static void main(String[] args) {
 *             
 *             OutputStream outputStream = new FileOutputStream("C:\\1dev\\training\\java\\javaee-demo"
        		+ "\\src\\com\\kpr\\training\\advanced\\output.txt");
 *             String content = "An apple is an edible fruit produced by an apple tree (Malus domestica). "
        		+ "Apple trees are cultivated worldwide and are the most widely grown species in the genus Malus."
        		+ "The tree originated in Central Asia, where its wild ancestor, Malus sieversii, is still found today."
        		+ "Apples have been grown for thousands of years in Asia and Europe and were brought to North America by European colonists.";
 *             //convert the string into input stream
 *             outputStream.write(inputStream);
 *             outputStream.close();
 *         }
 *     }
 */

package com.kpr.training.advanced;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamDemo {


    public static void main(String[] args) throws IOException {
       
    	OutputStream outputStream = new FileOutputStream("C:\\1dev\\training\\java\\javaee-demo"
        		+ "\\src\\com\\kpr\\training\\advanced\\output.txt");
        String content = "An apple is an edible fruit produced by an apple tree (Malus domestica). "
        		+ "Apple trees are cultivated worldwide and are the most widely grown species in the genus Malus."
        		+ "The tree originated in Central Asia, where its wild ancestor, Malus sieversii, is still found today."
        		+ "Apples have been grown for thousands of years in Asia and Europe and were brought to North America by European colonists."; 		
        byte[] inputStream = content.getBytes();
        outputStream.write(inputStream);
        outputStream.close();
        System.out.println("Successfully written");
    }
}