/*
Requirement:
    What is Method Reference and its types.Write a program for each types 
with suitable comments.


Entity:
    MethodReferenceTypes
Function Declaration:
    public static void print(final int number)
    public static void main(String[] args)

Jobs to be done:
    1)Declare a List of type integer named lista nd store elements in it.
    2)For each number 
      2.1)Invoke the method print and print the number.   
    3)For each number 
      3.1)Invoke the method print and print the number. 
    4)For each number 
      4.1)Invoke the method print and print the number. 
    5)For each number 
      5.1)Invoke the method print and print the number. 
    
pseudo code:
public class MethodReferenceTypes {
    
    public static void main(String[] args) {
        
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        
        list.forEach( MethodReferenceTypes::print);        
        list.forEach(number ->  MethodReferenceTypes.print(number));
        for(int number : list) {
            MethodReferenceTypes.print(number);
        }
    }
    
    public static void print(final int number) {
        System.out.println("The values : " + number);
    }  
}    


*/
/*
They are effectively a subset of lambda expressions, because if a lambda expression can be used,
then it might be possible to use a method reference, but not always. They can only be used to call 
singular method, which obviously reduces the possible places they can be used, unless your code is
written to cater for them. It would be a good idea if you knew the notation for a method reference.

Four Types:
    1. Reference to a static method
    2. Reference to an instance method of a particular object
    3. Reference to an instance method of an arbitrary object of a particular type
    4. Reference to a constructor
*/
package com.kpr.training.lambda;

import java.util.List;
import java.util.Arrays;

public class MethodReferenceTypes {
    
    // Reference to a static method
    
    public static void main(String[] args) {
        
    	List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        
        // Method reference 
        
        list.forEach( MethodReferenceTypes::print);
        
        // Lambda expression
        
        list.forEach(number ->  MethodReferenceTypes.print(number));
        
        // normal
        
        for(int number : list) {
            MethodReferenceTypes.print(number);
        }
    }
    
    public static void print(final int number) {
        System.out.println("The values : " + number);
    }  
}    

