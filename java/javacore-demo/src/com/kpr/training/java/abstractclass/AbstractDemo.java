/*
Requirement:
    Demonstrate abstract classes using Shape class.
       - Shape class should have methods to calculate area and perimeter
       - Define two more class Circle and Square by extending Shape class and implement the calculation
         for each class respectively
Entity:
    Square,Circle,Shape,AbstractDemo
Function Declaration:
    public void printArea()
    public void printPerimeter() 
    public void printPerimeter()

Jobs To be Done:
    1)Get the side as input from the user.
    2)Get the radius as input from the user.
    3)Pass the side as parameter for the constructor in the class Square.
      3.1)assign the value of side to side in entity square.
    4)Pass the radius as parameter for the constructor in the class Circle.
      4.1)assign the value of radius to radius in entity Circle.
    5)Invoke the method printArea from the abstract class Shape for the class Square .
      5.1)print the area of the square . 
    6)Invoke the method printPerimeter from the abstract class Shape for the class Square .
      6.1)print the perimeter of the square .
    7)Invoke the method printArea from the abstract class Shape for the class Circle .
      7.1)print the area of the square . 
    8)Invoke the method printPerimeter from the abstract class Shape for the class circle .
      8.1)print the perimeter of the Circle .
    9)Close the scanner.
*/

package com.kpr.training.java.abstractclass;

import java.util.Scanner;

abstract class Shape {
    public abstract void printArea();
    public abstract void printPerimeter();
}


class Square extends Shape {
    
    private double side;
    
    public Square(double side) {
        this.side = side;
    }
    
    public void printArea() {
        System.out.println("area of square = " + (side*side));
    }
    
    public void printPerimeter() {
        System.out.println("perimeter of square = " + (4 * side));
    }
}

class Circle extends Shape {
    
    private double radius; 
    
    public Circle(double radius) {
        this.radius = radius;
    }  
    
    public void printArea() {
        System.out.println("area of circle is = " + (3.14 * radius * radius));
    } 
    
    public void printPerimeter() {
        System.out.println("perimeter of circle = " + (2 * 3.14 * radius));
    }
}

public class AbstractDemo {
    
    public static void main(String[] args) {
    
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the lenght of the side of the square:");
        double side = scanner.nextDouble();
        System.out.print("Enter the radius of the circle:");
        double radius = scanner.nextDouble();
        Square square = new Square(side);
        square.printArea();
        square.printPerimeter();
        Circle circle = new Circle(radius);
        circle.printArea();
        circle.printPerimeter();
   }
}