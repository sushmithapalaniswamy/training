/*Requirement: To create program  for url class and url connection class in networking.
 * 
 * Entity: UrlAndUrlClassesDemo
 * 
 * Method Signature: public static void main(String[] args);
 * 
 * Jobs to be done: 1. Create Url for a web address.
 * 					2. Find and print the Protocol for url.
 * 					3. Find and print the port for url.
 * 					4. Find and print the file name for url.
 * 					5. Find and print the host of url.
 * 					6. Create a url connection for a web address.
 * 					7. Create a inputStream for the url connection.
 * 					8. Find all the data values of the web pages and print it in console.
 * PseudoCode:
 * 
public class UrlAndUrlClassesDemo {
	
	public static void main(String[] args)  {  
        
		try {  
            
			URL url = new URL("https://www.geeksforgeeks.org/stream-in-java");  
            System.out.println("Protocol: " + url.getProtocol());  
            System.out.println("Port Number: " + url.getPort());  
            System.out.println("File Name: " + url.getFile());  
            System.out.println("Host Name: " + url.getHost());   
            
            URL urlConn = new URL("https://www.geeksforgeeks.org/stream-in-java");  
            URLConnection ucon = urlConn.openConnection();  
            InputStream inputStream = ucon.getInputStream();  
            int i;  
            while ((i = inputStream.read()) != -1)  {  
            	System.out.print((char) i);  
            }  
		} catch (Exception ex)  {  
            System.out.println(ex);  
        }  
	}  
}  


 */
package com.kpr.training.networking;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class UrlAndUrlClassesDemo {
	
	public static void main(String[] args) throws IOException  {  
        
		URL url = new URL("https://www.geeksforgeeks.org/stream-in-java");  
        System.out.println("Protocol: " + url.getProtocol());  
        System.out.println("Port Number: " + url.getPort());  
        System.out.println("File Name: " + url.getFile());  
        System.out.println("Host Name: " + url.getHost());   
            
        URL urlConn = new URL("https://www.geeksforgeeks.org/stream-in-java");  
        URLConnection ucon = urlConn.openConnection();  
        InputStream inputStream = ucon.getInputStream();  
        int i;  
        while ((i = inputStream.read()) != -1)  {  
        	System.out.print((char) i);  
        }  
	}  
}  

