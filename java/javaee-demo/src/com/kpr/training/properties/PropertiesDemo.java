/* Requirement:
 *       Write a program to perform the following operations in Properties.
 *           i) add some elements to the properties file.
 *          ii) print all the elements in the properties file using iterator.
 *         iii) print all the elements in the properties file using list method.
 *     
 * Entity:
 *       PropertiesDemo
 *   
 * Function Signature:
 *       public static void main(String[] args)
 *   
 * Jobs to be done:
 *       1)Create a object for Property named property.
 *       2)Add elements to the property
 *       3)Create a object for FileOutputStream and assign a name for property file.
 *       4)Store the property in FileOutputStream as property file.
 *       5)Create an iterator for property file and store it in iterator.
 *       6)For each element in property
 *          6.1)Store the key in key
 *          6.2)Store the value in value
 *          6.3)Print the key and value.
 *       7)Print the property using list.
 *       
 * Pseudo code:
 * 
 public class PropertiesDemo {

    public static void main(String[] args) throws IOException{
    	
    	Properties property = new Properties();
		
		//add elements to the property

        OutputStream output = new FileOutputStream("Number.properties");
        property.store(output , null);
        

        Iterator<Object> iterator = property.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            String value = property.getProperty(key);
            System.out.println(key + " = " + value);
        }
        property.list(System.out);
    }
}
*/
package com.kpr.training.properties;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Properties;

public class PropertiesDemo {

    public static void main(String[] args) throws IOException{
    	
    	Properties property = new Properties();
		property.setProperty("1", "One");
		property.setProperty("2", "Two");
		property.setProperty("3", "Three");
		property.setProperty("4", "Four");


        OutputStream output = new FileOutputStream("Number.properties");
        property.store(output , null);
        

        System.out.println("Printing all the elements in the property file using iterator");
        Iterator<Object> iterator = property.keySet().iterator();
        while (iterator.hasNext()) {
           
        	String key = (String) iterator.next();
            String value = property.getProperty(key);
            System.out.println(key + " = " + value);
        }
        property.list(System.out);
    }
}